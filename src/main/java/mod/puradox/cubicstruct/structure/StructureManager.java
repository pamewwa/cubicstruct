package mod.puradox.cubicstruct.structure;

import mod.puradox.cubicstruct.CubicStruct;
import mod.puradox.cubicstruct.json.JsonUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.datafix.FixTypes;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.nio.file.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

public final class StructureManager { //Manages and contains all structures of the mod.
    public static File structureDir = new File("./cubic_structures/");
    public static List<StructureData> structures;
    public static List<StructureGroup> structureGroups;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void initStructures() throws IOException {
        Instant start = Instant.now();
        CubicStruct.LOGGER.info("Loading structures...");

        structures = new ArrayList<>(); //Initialize/reset loaded structures for initialization/reloading.
        structureGroups = new ArrayList<>();

        if(!structureDir.exists()) {
            Files.createDirectory(Paths.get(String.valueOf(structureDir))); //Create main structures directory if it doesn't exist.
            CubicStruct.LOGGER.info("\t\tMissing `cubic_structures` directory. Creating it...");
        } else if (!isDirEmpty(structureDir.toPath())) { //Initiate and register structures if directory has any.
            Arrays.stream(Objects.requireNonNull(structureDir.listFiles())).iterator().forEachRemaining((File file) -> {
                try { //For every file in main structure directory.
                    if(file.isDirectory() && !isDirEmpty(file.toPath())) { //If it's a directory, iterate over it to gather structure data.
                        CubicStruct.LOGGER.info("\t\t{}...", file.getName());
                        if (new File(file + "/group.json").exists()) { //Load as group if it's a group.
                            StructureGroup group = JsonUtils.readStructureGroup(new File(file + "/group.json")); //Initialize base group.
                            Arrays.stream(Objects.requireNonNull(file.listFiles())).iterator().forEachRemaining((File possibleJson) -> { //Iterate over each .json and add its structure to the group.
                                if (FilenameUtils.getExtension(possibleJson.getName()).contains("json") && !possibleJson.getName().equals("group.json")) {
                                    CubicStruct.LOGGER.info("\t\t\t{}...", possibleJson.getName());
                                    try {
                                        StructureData struct = JsonUtils.readStructureData(possibleJson); //Read from json.
                                        CubicStruct.LOGGER.info("\t\t\t\t{}{}{}", struct.getName(), " is ", struct.isEnabled() ? "enabled.":"disabled.");
                                        group.getStructures().add(struct); //Add structure to group.
                                    } catch (FileNotFoundException e) {
                                        throw new RuntimeException(e);
                                    }
                                } //The nbt's filename is contained within the structure data. Thus, only loading it is necessary.

                            });
                            group.initializeOrigins(); //Initialize origins into proper structures, for the group.
                            structureGroups.add(group); //Append the group after initialization.
                        } else if (new File(file + "/complex.json").exists()) { //Load as complex if it's a complex.
                            //Unimplemented
                        } else { //If structure if a singleton, load as usual.
                            Arrays.stream(Objects.requireNonNull(file.listFiles())).iterator().forEachRemaining((File possibleJson) -> {
                                if (FilenameUtils.getExtension(possibleJson.getName()).contains("json")) {
                                    try {
                                        StructureData struct = JsonUtils.readStructureData(possibleJson); //Read from json.
                                        CubicStruct.LOGGER.info("\t\t\t\t{}{}{}", struct.getName(), " is ", struct.isEnabled() ? "enabled.":"disabled.");
                                        structures.add(struct); //Append structure to list.
                                    } catch (FileNotFoundException e) {
                                        throw new RuntimeException(e);
                                    }
                                } //The nbt's filename is contained within the structure data. Thus, only loading it is necessary.
                            });
                        }
                    } else if (FilenameUtils.getExtension(file.getName()).contains("nbt")) { //If it's a structure nbt, initialize it so it may be used.
                        CubicStruct.LOGGER.info("\t\t{} {}{}", "Found uninitialized", file.getName(), ". Generating default data...");
                        Path newDir = Paths.get(("./cubic_structures/cubicstruct_" + FilenameUtils.removeExtension(file.getName()))); //The directory the data will be/is assigned to.
                        if(!new File(String.valueOf(newDir)).exists()) { //Initialize defaults if file doesn't exist.
                            Files.createDirectory(newDir);
                            Files.copy(file.toPath(), Paths.get(newDir + "/" + file.getName())); //Copy nbt into new directory.
                            JsonUtils.writeDefaultStructure(new File(newDir + "/" + FilenameUtils.removeExtension(file.getName()) + ".json")); //Write default data regarding how the structure spawns.
                        } else { //If the directory already exists, replace the existing .nbt.
                            CubicStruct.LOGGER.info("\t\t\t\t{} {}", file.getName(), " already has a generated directory. Replacing old .nbt...");
                            for(StructureData struct: structures) {
                                if(struct.getNbtStructure().getName().equals(file.getName())) {
                                    structures.remove(struct);
                                    break;
                                }
                            } //Remove old structure from list.
                            Files.copy(file.toPath(), Paths.get(newDir + "/" + file.getName()), StandardCopyOption.REPLACE_EXISTING);
                            structures.add(JsonUtils.readStructureData(new File(newDir + "/" + FilenameUtils.removeExtension(file.getName()) + ".json"))); //Use existing .json to reload the structure.
                        }
                        file.delete(); //File is now in functional directory. Old .nbt is no longer needed.
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }

        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        CubicStruct.LOGGER.info("Load task complete, took " + timeElapsed + "ms");
    }

    @SuppressWarnings({"BooleanMethodIsAlwaysInverted"})
    private static boolean isDirEmpty(final Path directory) throws IOException {
        try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory)) {
            return !dirStream.iterator().hasNext(); //If hasNext() is false on first call, the directory is empty.
        }
    }
    public static NBTTagCompound fileToNBT (File file) throws IOException {
        //Used as a substitute for TemplateManager's methods to function without a ResourceLocation
        if(file.exists() && FilenameUtils.getExtension(String.valueOf(file)).contains("nbt")) {
            InputStream stream = Files.newInputStream(file.toPath());
            NBTTagCompound nbtTagCompound = CompressedStreamTools.readCompressed(stream);
            if (!nbtTagCompound.hasKey("DataVersion", 99))
            {
                nbtTagCompound.setInteger("DataVersion", 500);
            }
            if (CubicStruct.side.isClient()) {
                nbtTagCompound = Minecraft.getMinecraft().getDataFixer().process(FixTypes.STRUCTURE, nbtTagCompound);
            } //No idea how to attain the datafixer on the server without loading a world first, which obviously cannot be done until structures are initialized. Perhaps there is some worldgen stage where this can be done, but I've no idea.
            return nbtTagCompound;
        } else {
            throw new FileNotFoundException("File " + file + " does not exist!");
        }

    }
}
