package mod.puradox.cubicstruct.worldgen;

import com.google.gson.*;
import io.github.opencubicchunks.cubicchunks.api.util.CubePos;
import io.github.opencubicchunks.cubicchunks.api.worldgen.populator.ICubicPopulator;
import mod.puradox.cubicstruct.CubicStruct;
import mod.puradox.cubicstruct.structure.StructureData;
import mod.puradox.cubicstruct.structure.StructureGroup;
import mod.puradox.cubicstruct.structure.StructureManager;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityMobSpawner;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.structure.template.PlacementSettings;
import net.minecraftforge.common.DungeonHooks;
import net.minecraftforge.fml.common.registry.VillagerRegistry;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.*;

import static mod.puradox.cubicstruct.worldgen.GeneratorUtils.*;

public class CubicStructureGenerator implements ICubicPopulator {
    @Override
    public void generate(World world, Random random, CubePos pos, Biome biome) {
        for(StructureGroup group: StructureManager.structureGroups) {
            if(attemptGroupGenerate(world, random, pos, biome, group)) {break;} //Don't generate more than one structure group in a chunk. Odds are, there isn't sufficient space, or it will cause a theme conflict.
        }
        StructureManager.structures.forEach(structure -> attemptGenerate(world, random, pos, biome, structure));
    }

    public static boolean attemptGenerate(World world, Random rand, CubePos chunk, Biome biome, StructureData structure) { //Make a generation attempt.
        if(structure.isEnabled()
                && rand.nextDouble()*100<structure.getSpawnChance()
                && Arrays.stream(structure.getDimensions()).anyMatch(x -> x == world.provider.getDimension())
                && (!structure.usesBiomeWhiteList() || Arrays.stream(structure.getBiomes()).anyMatch(x -> x == biome))
                && (!structure.usesBiomeBlackList() || Arrays.stream(structure.getBiomes()).noneMatch(x -> x == biome))
                && chunk.getY()*16 >= structure.getMinY()
                && chunk.getY()*16+15 <= structure.getMaxY() //Assuming value is given from bottom.
                && ((Math.abs(chunk.getX())*16 >= structure.getMinXZ()
                && Math.abs(chunk.getX())*16+15 <= structure.getMaxXZ())
                || (Math.abs(chunk.getZ())*16 >= structure.getMinXZ()
                && Math.abs(chunk.getZ())*16+15 <= structure.getMaxXZ()))
        ) {
            PlacementSettings settings = structure.getSettings();
            if(structure.isMirrorable()) {settings.setMirror(randomMirror(rand));}
            if(structure.isRotatable()) {settings.setRotation(randomRotation(rand));}
            BlockPos attemptPos = findPosition(world, rand, chunk, structure, settings);
            if(attemptPos==null) {return false;}

            if(structure.usesFillBlock()) {
                GeneratorUtils.fillFoundation(world, GeneratorUtils.getStartPos(attemptPos, structure.transformedSize(settings.getRotation()), settings.getRotation(), settings.getMirror()).add(0,structure.getPlacementType().equals("ceiling") ? structure.getSurfaceLevel():-structure.getSurfaceLevel(),0), structure.transformedSize(settings.getRotation()), structure.getFillBlock().getDefaultState(), structure.getPlacementType().equals("ceiling"));
            } //Fill foundations if structure enabled this feature.

            structure.addBlocksToWorld(world, attemptPos, settings);
            finalizeGeneration(world, structure, rand, attemptPos, settings);



            //System.out.println(structure.getName() + " spawned at " + attemptPos + " with " + settings.getMirror() + " and " + settings.getRotation());
            return true;
        }
        return false;
    }

    public static boolean attemptGroupGenerate(World world, Random rand, CubePos chunk, Biome biome, StructureGroup group) {
        StructureData activeOrigin = group.getOrigins().get(rand.nextInt(group.getOrigins().size()));
        for(int i = 0; i < group.getOrigins().size(); i++) { //At most, three attempts to find an appropriate origin.
            if(i==4) {break;}
            activeOrigin = group.getOrigins().get(rand.nextInt(group.getOrigins().size()));
            if(activeOrigin.isEnabled()
                    && Arrays.stream(activeOrigin.getDimensions()).anyMatch(x -> x == world.provider.getDimension())
                    && (!activeOrigin.usesBiomeWhiteList() || Arrays.stream(activeOrigin.getBiomes()).anyMatch(x -> x == biome))
                    && (!activeOrigin.usesBiomeBlackList() || Arrays.stream(activeOrigin.getBiomes()).noneMatch(x -> x == biome))
                    && chunk.getY()*16 >= activeOrigin.getMinY()
                    && chunk.getY()*16+15 <= activeOrigin.getMaxY() //Assuming value is given from bottom.
                    && ((Math.abs(chunk.getX())*16 >= activeOrigin.getMinXZ()
                    && Math.abs(chunk.getX())*16+15 <= activeOrigin.getMaxXZ())
                    || (Math.abs(chunk.getZ())*16 >= activeOrigin.getMinXZ()
                    && Math.abs(chunk.getZ())*16+15 <= activeOrigin.getMaxXZ()))
            ) {break;}
            activeOrigin=null;
        }
        if(activeOrigin==null) {return false;} //Null is returned from above loop if invalid.
        if(rand.nextDouble()*100<activeOrigin.getSpawnChance()) { //Add the origin before anything else.
            PlacementSettings settings = activeOrigin.getSettings();
            if(activeOrigin.isMirrorable()) {settings.setMirror(randomMirror(rand));}
            if(activeOrigin.isRotatable()) {settings.setRotation(randomRotation(rand));}
            BlockPos attemptPos = findPosition(world, rand, chunk, activeOrigin, settings);
            if(attemptPos==null) {return false;}
            //Not adding origin to world yet. Will do so if at least one other structure can spawn.


            int queueSize = Math.max(group.getMinSize(), rand.nextInt(group.getMaxSize()+1)); //How many structures will be spawned for the group.
            int totalWeight = 0; //Will be assigned after iterating.
            Map<StructureData, Integer> spawnData = new HashMap<>(); //How many of each to spawn.
            Map<Integer, StructureData> weightData = new HashMap<>(); //Weight threshold data.

            for(StructureData structure: group.getStructures()) { //Gather minimum spawn quantity data, and accumulate weights.
                if(structure.isEnabled()) {
                    spawnData.put(structure, structure.getMinCount()); //Assign mincount first, as they are required regardless of weight.
                    for (int thres = totalWeight; thres < totalWeight + structure.getWeight(); thres++) {
                        weightData.put(thres, structure);
                    }
                    totalWeight += structure.getWeight();

                    queueSize -= structure.getMinCount(); //Size has been allocated. Remove from queue.
                }
            }

            int l = 0;
            while(queueSize>0 && l<1000) { //Fill the remaining spawn data.
                StructureData selectedStructure = weightData.get(rand.nextInt(totalWeight)); //Gather structure according to weight.
                if(selectedStructure.getMaxCount()>spawnData.get(selectedStructure)) { //Don't append structures beyond their maximum count.
                    spawnData.replace(selectedStructure, spawnData.get(selectedStructure)+1); //Add structure to confirmed-gen queue.
                    queueSize--; //Decrement size queue.
                } else {l++;}
            }
            if(l>999) {CubicStruct.LOGGER.warn("{}{}{}","Attempting to fill queue for ", group.getName(), " failed 1000 iterations. Consider increasing the maximum count of structures, or lowering maximum group size.");}


            //Now for the actual generation
            int success = 0; //How many instances of generation succeeded, excluding origin.
            BlockPos attemptPos2;
            PlacementSettings settings2;

            Map<BlockPos, BlockPos> coordList = new HashMap<>(); //Position:Size - to determine structure-structure collisions.

            for(StructureData structure: group.getStructures()) { //Spawn everything up to its minimum count, as it receives priority.
                if(structure.getMinCount()>0) {
                    spawnData.replace(structure, spawnData.get(structure)- activeOrigin.getMinCount()); //Mincount is being spawned now, therefore remove them from queue.
                    for(int i = 0; i < structure.getMinCount(); i++) {
                        settings2 = structure.getSettings();
                        if(structure.isMirrorable()) {settings2.setMirror(randomMirror(rand));}
                        if(structure.isRotatable()) {settings2.setRotation(randomRotation(rand));}
                        attemptPos2 = findGroupStructurePosition(world, rand, chunk, structure, settings2, group, coordList);
                        if(attemptPos2==null) {continue;}

                        BlockPos startPos = GeneratorUtils.getStartPos(attemptPos2, structure.transformedSize(settings2.getRotation()), settings2.getRotation(), settings2.getMirror());

                        if(structure.usesFillBlock()) {
                            GeneratorUtils.fillFoundation(world, startPos.add(0,structure.getPlacementType().equals("ceiling") ? structure.getSurfaceLevel():-structure.getSurfaceLevel(),0), structure.transformedSize(settings2.getRotation()), structure.getFillBlock().getDefaultState(), structure.getPlacementType().equals("ceiling"));
                        } //Fill foundations if structure enabled this feature.
                        structure.addBlocksToWorld(world, attemptPos2, settings2);
                        finalizeGeneration(world, structure, rand, attemptPos2, settings2);

                        coordList.put(startPos.add(-group.getSpacing(),-group.getSpacing(),-group.getSpacing()), structure.transformedSize(settings2.getRotation()).add(group.getSpacing(),group.getSpacing(),group.getSpacing())); //Add spacing-adjusted coordinate
                        success++;
                    }
                }
            }

            for(StructureData structure: group.getStructures()) { //Spawn everything else.
                for(int i = 0; i < spawnData.get(structure); i++) {
                    settings2 = structure.getSettings();
                    if(structure.isMirrorable()) {settings2.setMirror(randomMirror(rand));}
                    if(structure.isRotatable()) {settings2.setRotation(randomRotation(rand));}
                    attemptPos2 = findGroupStructurePosition(world, rand, chunk, structure, settings2, group, coordList);
                    if(attemptPos2==null) {continue;}

                    BlockPos startPos = GeneratorUtils.getStartPos(attemptPos2, structure.transformedSize(settings2.getRotation()), settings2.getRotation(), settings2.getMirror());

                    if(structure.usesFillBlock()) {
                        GeneratorUtils.fillFoundation(world, startPos.add(0,structure.getPlacementType().equals("ceiling") ? structure.getSurfaceLevel():-structure.getSurfaceLevel(),0), structure.transformedSize(settings2.getRotation()), structure.getFillBlock().getDefaultState(), structure.getPlacementType().equals("ceiling"));
                    } //Fill foundations if structure enabled this feature.
                    structure.addBlocksToWorld(world, attemptPos2, settings2);
                    finalizeGeneration(world, structure, rand, attemptPos2, settings2);

                    coordList.put(startPos.add(-group.getSpacing(),-group.getSpacing(),-group.getSpacing()), structure.transformedSize(settings2.getRotation()).add(group.getSpacing(),group.getSpacing(),group.getSpacing()));
                    success++;
                }
            }

            if(success>0) { //Don't add origin if nothing else placed successfully; it wouldn't be much a group then.
                if(activeOrigin.usesFillBlock()) {
                    GeneratorUtils.fillFoundation(world, GeneratorUtils.getStartPos(attemptPos, activeOrigin.transformedSize(settings.getRotation()), settings.getRotation(), settings.getMirror()).add(0,activeOrigin.getPlacementType().equals("ceiling") ? activeOrigin.getSurfaceLevel():-activeOrigin.getSurfaceLevel(),0), activeOrigin.transformedSize(settings.getRotation()), activeOrigin.getFillBlock().getDefaultState(), activeOrigin.getPlacementType().equals("ceiling"));
                } //Fill foundations if structure enabled this feature.
                activeOrigin.addBlocksToWorld(world, attemptPos, settings); //Adding origin to world.
                finalizeGeneration(world, activeOrigin, rand, attemptPos, settings);
                //System.out.println(activeOrigin.getName() + " spawned at " + GeneratorUtils.getStartPos(attemptPos, activeOrigin.transformedSize(settings.getRotation()), settings.getRotation(), settings.getMirror()).toString() + ", using rotation " + settings.getRotation() + " and mirror " + settings.getMirror());
                return true;
            }
        }
        return false;
    }

    public static BlockPos findGroupStructurePosition(World world, Random rand, CubePos chunk, StructureData structure, PlacementSettings settings, StructureGroup group, Map<BlockPos, BlockPos> coordList) {
        int attempts = 0;
        int rangeY = ((chunk.getY()-group.getMaxSpreadY())*16); //Get the very bottom position allowed for spawning.
        while (attempts < 9) {
            int[] chunkPlacement = new int[3];
            for (int n = 0; n < 3; n++) {
                if(n==1) {chunkPlacement[n] = rand.nextInt(group.getMaxSpreadY()+1)*((rand.nextInt(2) * 2) - 1);} else {
                    chunkPlacement[n] = (rand.nextInt(group.getMaxSpreadXZ())+1)*((rand.nextInt(2) * 2) - 1);
                } //+1 as rand is exclusive, to ensure a structure will never spawn in the same xz as origin.
            } //Random coordinates relative to origin for each axis, which determine chunk to spawn in relative to origin.
            BlockPos pos = new BlockPos( //Join relative coordinates with the exact position of origin
                    ((chunk.getX()+chunkPlacement[0]) * 16) + rand.nextInt(16),
                    Math.min(Math.max(((chunk.getY()+chunkPlacement[1]) * 16) - rand.nextInt(group.getMaxSpreadY()*16), structure.getMinY()), structure.getMaxY()),
                    ((chunk.getZ()+chunkPlacement[2]) * 16) + rand.nextInt(16)
            );
            if(Objects.equals(structure.getPlacementType(), "force") && checkStructureCollision(getStartPos(pos, structure.transformedSize(settings.getRotation()), settings.getRotation(), settings.getMirror()), structure.transformedSize(settings.getRotation()), coordList, structure.isLenient(), false, 0)) {
                if (structure.isAirSpawn() && world.getBlockState(pos).getBlock().equals(Blocks.AIR)) {return pos;}
                else if (structure.isLiquidSpawn() && world.getBlockState(pos).getMaterial().isLiquid()) {return pos;}
                else if (!(structure.isAirSpawn() || structure.isLiquidSpawn())) {return pos;}
            } else if(Objects.equals(structure.getPlacementType(), "block")) {
                if (world.getBlockState(pos).getBlock().equals(structure.getReplaced())
                    && world.getBlockState(pos.add(0,structure.getSize().getY(),0)).getBlock().equals(structure.getReplaced())
                    && ((!structure.isFloating()) || isFloating(world,pos,structure.getReplaced()))
                    && checkStructureCollision(getStartPos(pos, structure.transformedSize(settings.getRotation()), settings.getRotation(), settings.getMirror()), structure.transformedSize(settings.getRotation()), coordList, structure.isLenient(), false, 0)) {
                    return pos;
                }
            } else if (Objects.equals(structure.getPlacementType(), "surface")) {
                while(pos.getY()>rangeY) {//If location cannot be found within this bounds, it would spawn outside max spread.
                    if(world.getBlockState(pos).getMaterial().blocksMovement() && !world.getBlockState(pos.up()).getMaterial().blocksMovement()) {break;}
                    pos = pos.down();
                } //Iterating downwards until solid block with non-solid block above is detected.
                if(world.getBlockState(pos).getMaterial().blocksMovement()
                        && pos.getY()>rangeY
                        && (!structure.isLiquidSpawn() || world.getBlockState(pos.up()).getMaterial().isLiquid())
                        && (!structure.isAirSpawn() || world.getBlockState(pos.up()).getBlock().equals(Blocks.AIR))
                        && GeneratorUtils.isSurfaceValid(world, structure, pos, settings.getRotation(), settings.getMirror(), false)
                        && checkStructureCollision(getStartPos(pos, structure.transformedSize(settings.getRotation()), settings.getRotation(), settings.getMirror()), structure.transformedSize(settings.getRotation()), coordList, structure.isLenient(), false, structure.getSurfaceLevel())) {
                    return new BlockPos(pos.getX(),pos.up().getY()+structure.getSurfaceLevel(), pos.getZ()); //Return the position upwards, which is the surface level, and add by how high/deep it is.
                }
            } else if (Objects.equals(structure.getPlacementType(), "liquidsurface")) {
                while(pos.getY()>rangeY) { //If location cannot be found within this bounds, it would spawn outside max spread.
                    if(world.getBlockState(pos).getMaterial().isLiquid()) {
                        if(world.getBlockState(pos.up()).getBlock().equals(Blocks.AIR)) {
                            break;
                        }
                        pos = pos.up();
                    }
                    pos = pos.down();
                } //Iterating downwards until liquid with air above is detected.
                if(pos.getY()>rangeY
                        && (world.getBlockState(pos).getMaterial().isLiquid())
                        && (world.getBlockState(pos.up()).getBlock().equals(Blocks.AIR))
                        && GeneratorUtils.isWaterSurfaceValid(world, structure, pos, settings.getRotation(), settings.getMirror())
                        && checkStructureCollision(getStartPos(pos, structure.transformedSize(settings.getRotation()), settings.getRotation(), settings.getMirror()), structure.transformedSize(settings.getRotation()), coordList, structure.isLenient(), false, structure.getSurfaceLevel())) {

                    return new BlockPos(pos.getX(), pos.up().getY()+structure.getSurfaceLevel(), pos.getZ()); //Return the position upwards, which is the surface level, and add by how high/deep it is.
                }
            } else if (Objects.equals(structure.getPlacementType(), "ceiling")) {
                int rangeTop = rangeY+group.getMaxSpreadY(); //Top of max spread.
                while(pos.getY()<rangeTop) { //If location cannot be found within this bounds, it would spawn outside max spread.
                    if(world.getBlockState(pos).getMaterial().blocksMovement() && !world.getBlockState(pos.down()).getMaterial().blocksMovement()) {break;}
                    pos = pos.up();
                } //Iterating upwards until solid block with non-solid block below is detected.
                if(world.getBlockState(pos).getMaterial().blocksMovement()
                        && pos.getY()<rangeTop
                        && (!structure.isLiquidSpawn() || world.getBlockState(pos.down()).getMaterial().isLiquid())
                        && (!structure.isAirSpawn() || world.getBlockState(pos.down()).getBlock().equals(Blocks.AIR))
                        && GeneratorUtils.isSurfaceValid(world, structure, pos, settings.getRotation(), settings.getMirror(), true)
                        && checkStructureCollision(getStartPos(pos, structure.transformedSize(settings.getRotation()), settings.getRotation(), settings.getMirror()), structure.transformedSize(settings.getRotation()), coordList, structure.isLenient(), true, structure.getSurfaceLevel())) {

                    return new BlockPos(pos.getX(), pos.getY()-structure.getSurfaceLevel()-structure.getSize().getY(), pos.getZ()); //Return position where the structure will spawn. `addBlocksToWorld` starts from the bottom corner, thus adjustment must be made. That normally it'd acquire pos.down() is offset by how getSize() is +1 over.
                }
            }
            attempts++;
        }
        return null;
    }

    public static BlockPos findPosition(World world, Random rand, CubePos chunk, StructureData structure, PlacementSettings settings) { //Find a position the structure can generate into.
        int attempts = 0;
        while (attempts < 9) {
            BlockPos pos = new BlockPos(
                    (chunk.getX() * 16) + rand.nextInt(16),
                    Math.min(Math.max((chunk.getY() * 16) - rand.nextInt(16), structure.getMinY()), structure.getMaxY()),
                    (chunk.getZ() * 16) + rand.nextInt(16));
            if(Objects.equals(structure.getPlacementType(), "force")) {
                if (structure.isAirSpawn() && world.getBlockState(pos).getBlock().equals(Blocks.AIR)) {return pos;}
                else if (structure.isLiquidSpawn() && world.getBlockState(pos).getMaterial().isLiquid()) {return pos;}
                else if (!(structure.isAirSpawn() || structure.isLiquidSpawn())) {return pos;}
            } else if(Objects.equals(structure.getPlacementType(), "block")) {
                if (world.getBlockState(pos).getBlock().equals(structure.getReplaced())
                    && world.getBlockState(pos.add(0,structure.getSize().getY(),0)).getBlock().equals(structure.getReplaced())
                    && ((!structure.isFloating()) || isFloating(world,pos, structure.getReplaced()))) {
                    return pos;
                }
            } else if (Objects.equals(structure.getPlacementType(), "surface")) {
                int chunkYOffset = 16-Math.abs(pos.getY()%16); //If location cannot be found within this bounds, it would spawn outside chunk.
                int shiftAttempts = 0;
                while(shiftAttempts<chunkYOffset) {
                    if(world.getBlockState(pos).getMaterial().blocksMovement() && !world.getBlockState(pos.up()).getMaterial().blocksMovement()) {break;}
                    pos = pos.down();
                    shiftAttempts++;
                } //Iterating downwards until solid block with non-solid block above is detected.
                if(world.getBlockState(pos).getMaterial().blocksMovement()
                        && shiftAttempts<chunkYOffset
                        && (!structure.isLiquidSpawn() || world.getBlockState(pos.up()).getMaterial().isLiquid())
                        && (!structure.isAirSpawn() || world.getBlockState(pos.up()).getBlock().equals(Blocks.AIR))
                        && GeneratorUtils.isSurfaceValid(world, structure, pos, settings.getRotation(), settings.getMirror(), false)) {
                    return new BlockPos(pos.getX(),pos.up().getY()+structure.getSurfaceLevel(), pos.getZ()); //Return the position upwards, which is the surface level, and add by how high/deep it is.
                }
            } else if (Objects.equals(structure.getPlacementType(), "liquidsurface")) {
                int chunkYOffset = 16-Math.abs(pos.getY()%16); //If location cannot be found within this bounds, it would spawn outside chunk.
                int shiftAttempts = 0;
                while(shiftAttempts<chunkYOffset) {
                    if(world.getBlockState(pos).getMaterial().isLiquid()) {
                        if(world.getBlockState(pos.up()).getBlock().equals(Blocks.AIR)) {
                            break;
                        }
                        pos = pos.up();
                    }
                    pos = pos.down();
                    shiftAttempts++;
                } //Iterating downwards until liquid with air above is detected.
                if(shiftAttempts<chunkYOffset
                        && (world.getBlockState(pos).getMaterial().isLiquid())
                        && (world.getBlockState(pos.up()).getBlock().equals(Blocks.AIR))
                        && GeneratorUtils.isWaterSurfaceValid(world, structure, pos, settings.getRotation(), settings.getMirror())) {

                    return new BlockPos(pos.getX(), pos.up().getY()+structure.getSurfaceLevel(), pos.getZ()); //Return the position upwards, which is the surface level, and add by how high/deep it is.
                }
            } else if (Objects.equals(structure.getPlacementType(), "ceiling")) {
                int chunkYOffset = Math.abs(pos.getY()%16); //If location cannot be found within this bounds, it would spawn outside chunk.
                int shiftAttempts = 0;
                while(shiftAttempts<chunkYOffset) {
                    if(world.getBlockState(pos).getMaterial().blocksMovement() && !world.getBlockState(pos.down()).getMaterial().blocksMovement()) {break;}
                    pos = pos.up();
                    shiftAttempts++;
                } //Iterating upwards until solid block with non-solid block below is detected.
                if(world.getBlockState(pos).getMaterial().blocksMovement()
                        && shiftAttempts<chunkYOffset
                        && (!structure.isLiquidSpawn() || world.getBlockState(pos.down()).getMaterial().isLiquid())
                        && (!structure.isAirSpawn() || world.getBlockState(pos.down()).getBlock().equals(Blocks.AIR))
                        && GeneratorUtils.isSurfaceValid(world, structure, pos, settings.getRotation(), settings.getMirror(), true)) {

                    return new BlockPos(pos.getX(), pos.getY()-structure.getSurfaceLevel()-structure.getSize().getY(), pos.getZ()); //Return position where the structure will spawn. `addBlocksToWorld` starts from the bottom corner, thus adjustment must be made. That normally it'd acquire pos.down() is offset by how getSize() is +1 over.
                }
            }
            attempts++;
        }
        return null;
    }
    @SuppressWarnings("deprecation")
    public static void finalizeGeneration (World world, StructureData structure, Random rand, BlockPos pos, PlacementSettings settings) { //Finalize structure by resolving data blocks.
        //Assuming the original attempt position, hopefully, as this method's map transforms inherently.

        Map<BlockPos, String> dataMap = structure.getDataBlocks(pos, settings); //Map of all data blocks and their positions.
        int baseSeed = rand.nextInt(); //Generate a consistent seed from the chunk's random.
        
        dataMap.forEach((dataPos, data) -> {
            String[] params = data.trim().split(" ");
            if(data.indexOf('{')!=-1) {
                String nbt = data.substring(data.indexOf('{'), data.lastIndexOf('}')); //A general position for any nbt, to remove spaces.
                String fixedData = data.trim().replace(nbt, nbt.replaceAll(" ", "")); //Trim and replace with fixed nbt.
                params = fixedData.split(" ");
            } else if (data.indexOf('[')!=-1) {
                String nbt = data.substring(data.indexOf('['), data.lastIndexOf(']')); //A general position for any nbt, to remove spaces.
                String fixedData = data.trim().replace(nbt, nbt.replaceAll(" ", "")); //Trim and replace with fixed nbt.
                params = fixedData.split(" ");
            }


            if(params[0].equalsIgnoreCase("villager")) {
                if(data.trim().equalsIgnoreCase("villager")) {randomVillager(world, dataPos, rand, null, null);}
                else if (NumberUtils.isCreatable(params[1])) {randomVillager(world, dataPos, rand, VillagerRegistry.getById(Integer.parseInt(params[1])), params.length>2 ? Integer.parseInt(params[2]):null);} //By numeric id.
                else if (params[1].equalsIgnoreCase("farmer")) {randomVillager(world, dataPos, rand, VillagerRegistry.getById(0), params.length>2 ? Integer.parseInt(params[2]):null);}
                else if (params[1].equalsIgnoreCase("librarian")) {randomVillager(world, dataPos, rand, VillagerRegistry.getById(1), params.length>2 ? Integer.parseInt(params[2]):null);}
                else if (params[1].equalsIgnoreCase("cleric")) {randomVillager(world, dataPos, rand, VillagerRegistry.getById(2), params.length>2 ? Integer.parseInt(params[2]):null);}
                else if (params[1].equalsIgnoreCase("blacksmith")) {randomVillager(world, dataPos, rand, VillagerRegistry.getById(3), params.length>2 ? Integer.parseInt(params[2]):null);}
                else if (params[1].equalsIgnoreCase("butcher")) {randomVillager(world, dataPos, rand, VillagerRegistry.getById(4), params.length>2 ? Integer.parseInt(params[2]):null);}
                else if (params[1].equalsIgnoreCase("nitwit")) {randomVillager(world, dataPos, rand, VillagerRegistry.getById(5), params.length>2 ? Integer.parseInt(params[2]):null);}
            } else if (params[0].equalsIgnoreCase("spawner")) {
                world.setBlockState(dataPos, Blocks.MOB_SPAWNER.getDefaultState(), 2);
                TileEntity tileentity = world.getTileEntity(dataPos);
                assert tileentity != null;
                ((TileEntityMobSpawner)tileentity).getSpawnerBaseLogic().setEntityId(DungeonHooks.getRandomDungeonMob(rand));
            } else if (params[0].equalsIgnoreCase("block")) {
                Random randomBlock = new Random();
                if(params.length>2) {randomBlock.setSeed((long) baseSeed * Integer.parseInt(params[2]));} //Set seed according to seed parameter.

                //String hell.
                if(params[1].contains("nbt:")) {
                    params[1] = params[1].substring(0, params[1].indexOf("nbt:")) + params[1].substring(params[1].indexOf("nbt:")).replace("\"", "\\\""); //Ensure sub-NBT quotes are actual quotes and not parsed by the main parser.
                    int lastNBTIndex = params[1].lastIndexOf("]}");
                    params[1] = params[1].replaceFirst("nbt:\\{", "nbt:\"{").substring(0, lastNBTIndex) + "\"}" + params[1].substring(lastNBTIndex); //Adding quotes to sub-NBT, so the parser wouldn't mistake it.
                }

                //Parsing json
                JsonObject obj = new JsonParser().parse(params[1]).getAsJsonObject(); //Parse second param as json.
                if(obj!=null) {
                    JsonArray jsonBlocks = obj.getAsJsonArray("blocks");

                    Map<IBlockState, NBTTagCompound> blocks = new HashMap<>();
                    try {
                        for (int i = 0; i < jsonBlocks.size(); i++) {
                            Block block = Block.REGISTRY.getObject(new ResourceLocation(jsonBlocks.get(i).getAsJsonObject().get("id").getAsString()));
                            IBlockState blockState = jsonBlocks.get(i).getAsJsonObject().get("data")==null ? block.getDefaultState() : block.getStateFromMeta(jsonBlocks.get(i).getAsJsonObject().get("data").getAsInt());
                            blocks.put(blockState, JsonToNBT.getTagFromJson(jsonBlocks.get(i).getAsJsonObject().get("nbt") == null ? "{}": jsonBlocks.get(i).getAsJsonObject().get("nbt").getAsString()));
                        }

                        int selected = randomBlock.nextInt(blocks.size()); //Selected index in list.
                        IBlockState selectedBlock = (IBlockState) blocks.keySet().toArray()[selected];
                        NBTTagCompound selectedNBT = blocks.get(selectedBlock);

                        world.setBlockState(dataPos, selectedBlock);

                        //Now we set nbt.
                        TileEntity tileentity = world.getTileEntity(dataPos);

                        if (tileentity != null)
                        {
                            selectedNBT.setInteger("x", dataPos.getX());
                            selectedNBT.setInteger("y", dataPos.getY());
                            selectedNBT.setInteger("z", dataPos.getZ());
                            tileentity.readFromNBT(selectedNBT);
                        }

                    } catch(Exception e) {
                        CubicStruct.LOGGER.error("Received invalid json from {}. Nbt is likely formatted incorrectly.", data);
                    }
                } else {
                    CubicStruct.LOGGER.error("Received null block list json from {}. Verify formatting?", data);
                }
            }
        });
    }
}
