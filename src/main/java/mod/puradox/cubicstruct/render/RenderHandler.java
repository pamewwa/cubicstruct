package mod.puradox.cubicstruct.render;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

import static org.lwjgl.opengl.GL11.GL_QUADS;

@Mod.EventBusSubscriber(Side.CLIENT)
public class RenderHandler {
    public static BlockPos renderSize = null;
    public static BlockPos renderPosition = null;

    @SubscribeEvent
    public void renderWorldLastEvent(RenderWorldLastEvent event) { //Used by CSViewer to preview a structure before spawning.
        //Thanks to https://gist.github.com/Cadiboo/7e46eb6e59d94c29d12dfdb087ea8b9f
        if (renderSize != null && renderPosition != null) {
            final float partialTicks = event.getPartialTicks();

            final Minecraft minecraft = Minecraft.getMinecraft();
            final Entity renderViewEntity = minecraft.getRenderViewEntity();
            assert renderViewEntity != null;
            // Interpolating everything back to (0, 0, 0). You can find these transforms in the RenderEntity class
            final double d0 = renderViewEntity.lastTickPosX + ((renderViewEntity.posX - renderViewEntity.lastTickPosX) * partialTicks);
            final double d1 = renderViewEntity.lastTickPosY + ((renderViewEntity.posY - renderViewEntity.lastTickPosY) * partialTicks);
            final double d2 = renderViewEntity.lastTickPosZ + ((renderViewEntity.posZ - renderViewEntity.lastTickPosZ) * partialTicks);
            final Tessellator tessellator = Tessellator.getInstance();
            final BufferBuilder bufferBuilder = tessellator.getBuffer();
            // Transform to BlockPos (0, 0, 0)
            bufferBuilder.setTranslation(-d0, -d1, -d2);

            GlStateManager.disableCull();
            GlStateManager.disableTexture2D();
            GlStateManager.enableBlend();
            GlStateManager.disableAlpha();

            bufferBuilder.begin(GL_QUADS, DefaultVertexFormats.POSITION_COLOR);


            //QUADS work in vertices of four each; every four vertices ends the draw, and you can begin a new rectangle where-ever.
            //Bottom
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();

            //Front
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();

            //Top
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();

            //Back
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();

            //Left
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY(), renderPosition.getZ()).color(250, 250, 250, 100).endVertex();

            //Right
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY()+renderSize.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX(), renderPosition.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();
            bufferBuilder.pos(renderPosition.getX()+renderSize.getX(), renderPosition.getY(), renderPosition.getZ()+renderSize.getZ()).color(250, 250, 250, 100).endVertex();

            tessellator.draw();

            bufferBuilder.setTranslation(0, 0, 0);
            GlStateManager.enableCull();
            GlStateManager.enableTexture2D();
            GlStateManager.disableBlend();
            GlStateManager.enableAlpha();
        }
    }
}
