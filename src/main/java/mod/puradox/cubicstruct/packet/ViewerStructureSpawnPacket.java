package mod.puradox.cubicstruct.packet;

import io.netty.buffer.ByteBuf;
import mod.puradox.cubicstruct.structure.StructureData;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.structure.template.Template;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import org.apache.commons.lang3.SerializationUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ViewerStructureSpawnPacket implements IMessage {
    private StructureData structure;
    private BlockPos position;

    public ViewerStructureSpawnPacket() {
    }

    public ViewerStructureSpawnPacket(BlockPos position, StructureData structure) {
        this.position = position;
        this.structure = structure;
    }

    @Override
    public void fromBytes(ByteBuf in) {
        //Acquire position.
        this.position = new BlockPos(in.readInt(), in.readInt(), in.readInt());

        //Convert remaining bytes to structure.
        int len = in.readInt();
        this.structure = SerializationUtils.deserialize(in.readBytes(len).array());
    }

    @Override
    public void toBytes(ByteBuf out) {
        //Write position.
        out.writeInt(position.getX());
        out.writeInt(position.getY());
        out.writeInt(position.getZ());

        try { //Write structure to bytes.
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(structure);
            oos.flush();
            byte[] data = bos.toByteArray();
            out.writeInt(data.length);
            out.writeBytes(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
