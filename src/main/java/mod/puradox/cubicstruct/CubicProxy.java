package mod.puradox.cubicstruct;

import io.github.opencubicchunks.cubicchunks.api.worldgen.CubeGeneratorsRegistry;
import mod.puradox.cubicstruct.worldgen.CubicStructureGenerator;

public class CubicProxy implements ModCompat{
    @Override
    public void addModCompat() {
        CubeGeneratorsRegistry.register(new CubicStructureGenerator(), 30); //Worldgen hook. Not entirely sure what 'weight' does, unfortunately.
    }
}
