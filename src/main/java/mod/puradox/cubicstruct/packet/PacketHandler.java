package mod.puradox.cubicstruct.packet;

import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.server.permission.PermissionAPI;

public class PacketHandler implements IMessageHandler<ViewerStructureSpawnPacket, IMessage> {
    @Override
    public IMessage onMessage(ViewerStructureSpawnPacket message, MessageContext ctx) {
        if(PermissionAPI.hasPermission(ctx.getServerHandler().player, "cubicstruct.spawnStructure")) {
            System.out.println("CubicStruct packet received successfully!");
        } else {
            ctx.getServerHandler().player.sendMessage(new TextComponentString("Unable to spawn structure: lacking permissions."));
        }
        return null;
    }
}
