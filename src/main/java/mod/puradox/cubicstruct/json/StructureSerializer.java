package mod.puradox.cubicstruct.json;

import com.google.gson.*;
import mod.puradox.cubicstruct.structure.StructureData;
import net.minecraft.world.biome.Biome;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Objects;

public final class StructureSerializer implements JsonSerializer<StructureData> {
    @Override
    public JsonElement serialize(StructureData src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray dimensions = new JsonArray();
        JsonArray biomes = new JsonArray();
        Arrays.stream(src.getDimensions()).iterator().forEachRemaining((int dim) -> dimensions.add(dim));
        Arrays.stream(src.getBiomes()).iterator().forEachRemaining((Biome biome) -> biomes.add(Objects.requireNonNull(biome.getRegistryName()).toString()));

        JsonObject obj = new JsonObject();
        obj.addProperty("file", String.valueOf(src.getNbtStructure()));
        obj.addProperty("name", src.getName());
        obj.addProperty("description", src.getDescription());
        obj.addProperty("spawnChance", src.getSpawnChance());
        obj.addProperty("surfaceLevel", src.getSurfaceLevel());
        obj.add("dimensions", dimensions);
        obj.addProperty("biomeWhiteList", src.usesBiomeWhiteList());
        obj.addProperty("biomeBlackList", src.usesBiomeBlackList());
        obj.add("biomes", biomes);
        obj.addProperty("minY", src.getMinY());
        obj.addProperty("maxY", src.getMaxY());
        obj.addProperty("minXZ", src.getMinXZ());
        obj.addProperty("maxXZ", src.getMaxXZ());
        obj.addProperty("airSpawn", src.isAirSpawn());
        obj.addProperty("liquidSpawn", src.isLiquidSpawn());
        obj.addProperty("rotate", src.isRotatable());
        obj.addProperty("mirror", src.isMirrorable());
        obj.addProperty("maxDecay", src.getMaxDecay());
        obj.addProperty("placementType", src.getPlacementType());
        obj.addProperty("replaced", Objects.requireNonNull(src.getReplaced().getRegistryName()).toString());
        obj.addProperty("floating", src.isFloating());
        obj.addProperty("usesFillBlock", src.usesFillBlock());
        obj.addProperty("fillBlock", Objects.requireNonNull(src.getFillBlock().getRegistryName()).toString());
        obj.addProperty("lenient", src.isLenient());
        obj.addProperty("enabled", src.isEnabled());

        return obj;
    }
}
