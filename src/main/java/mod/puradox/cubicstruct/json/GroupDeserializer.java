package mod.puradox.cubicstruct.json;

import com.google.gson.*;
import mod.puradox.cubicstruct.structure.StructureGroup;

import java.lang.reflect.Type;

public class GroupDeserializer implements JsonDeserializer<StructureGroup> {
    @Override
    public StructureGroup deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject obj = json.getAsJsonObject(); //Acquire the object we're deserializing.

        String outName = obj.get("name") == null ? "Unnamed Group": obj.get("name").getAsString(); //Name may not be set.

        String[] origins = new String[obj.getAsJsonArray("origins").size()];
        if (obj.get("origins") != null) {
            int i = 0;
            for (JsonElement jsonItem : obj.getAsJsonArray("origins")) {
                origins[i] = jsonItem.getAsString();
                i++;
            }
        }

        try {
            StructureGroup data = new StructureGroup(outName, origins);

            if(obj.get("maxSpreadXZ")!=null){data.setMaxSpreadXZ(obj.get("maxSpreadXZ").getAsInt());}
            if(obj.get("maxSpreadY")!=null){data.setMaxSpreadY(obj.get("maxSpreadY").getAsInt());}
            if(obj.get("minSize")!=null){data.setMinSize(obj.get("minSize").getAsInt());}
            if(obj.get("maxSize")!=null){data.setMaxSize(obj.get("maxSize").getAsInt());}
            if(obj.get("spacing")!=null){data.setSpacing(obj.get("spacing").getAsInt());}
            if(obj.get("enabled")!=null){data.setEnabled(obj.get("enabled").getAsBoolean());}

            return data;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
