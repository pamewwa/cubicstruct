# About

CubicStruct is a mod intended to implement custom structures (nbt files) into world generation, with compatibility for Cubic World Gen. For each structure, it offers settings for biome, maximum decay, placement, and more through their respective .json files. If you're creative, perhaps you'll never need another structure mod. Additionally, for a means to implement custom loot tables into said structures without creating a mod or pasting into every world, see [Iloot](https://gitlab.com/Absthistle/iloot).

## Usage

### Vanilla/CC Compatibility Warnings

- If using Cubic Chunks without Cubic World Gen, minX and minY settings *must* be adjusted, or worldgen will attempt to spawn structures by tracing from the absurd top of the world.
- While not a required dependency, this mod was developed specifically for structure compatibility with Cubic World Gen; regular worlds with or without Cubic Chunks may produce unintended effects (such as failed spawns). For ideal results without CWG, set minX and minY to structure-appropriate values.

### Acquiring structure .nbt

To acquire any structure with .nbt format, simply save it using Minecraft's vanilla structure block. The new file will be found within the save's 'structures' directory, which you can then paste into the generated 'cubic_structures' folder, located in the base Minecraft directory.

#### Data Blocks

CubicStruct implements additional features for data blocks, which manifest on natural worldgen (unimplemented for manual loading) which are listed below (`<<>> - required`; `<> - optional`):
- `villager <(profession (string/int))> <(career (int))>` - spawns a random villager with the given profession and career.
  - Professions available are `farmer`, `librarian`, `cleric`, `blacksmith`, `butcher`, and `nitwit`. Alternatively, use id.
- `spawner` - generates a random monster spawner, from the dungeon pool.
- `block <<{blocks:[{<<id: (string)>>,<data: (int)>,<nbt: (nbt string)>}]}>> <(seed ((int))>` - generates a random block from the provided list. Providing a seed allows multiple blocks within a structure to generate similarly (only the blocks themselves, not for any nbt associated). A formatting example would be `block {blocks:[{id:"minecraft:gravel"},{id:"minecraft:chest",data:1,nbt:{LootTable:"iloot:chests/fish"}}]} 7`.



### Initializing the structure

#### General

The .nbt will be initialized on game/mod reload, and a new directory will be produced from it. The new directory will contain a .json which can be modified at will to alter how the structure spawns. The values are all preset, and determine spawn as follows:
- **file** - (default: filename, required) - the only required parameter which isn't auto-assumed (will still be auto-generated). Do not modify this unless missing, moving the .nbt into a group, or also altering the filename of whatever is being edit.
- **name** - (default: filename) - name for the structure.
- **description** - (default: "No description provided.") - brief description for the structure.
- **spawnChance** - (default: 1.0) - percent chance that an attempt will be made to spawn the structure within a chunk. Note that this value is relative, and may be impacted by generation failures; structures which are very likely to fail generating should be provided a higher value.
- **surfaceLevel** - (default: 0) - the Y level offset relative to the structure's height to begin spawning onto a surface/ceiling. When placementType is set to `surface`, `0` results in spawning from the bottom of the structure, while the reverse is true of `ceiling`. For example, if a structure extends 8 blocks into the ground ('ground' being whatever surface/ceiling it latches onto), this should be set to `-8`. `block` and `force` ignore this.
- **dimensions** - (default: [0]) - list of all dimensions this structure can spawn in (-1 = nether, 0 = overworld, 1 = end). This feature is currently nonfunctional in CWG worlds; use biome lists instead.
- **biomeWhiteList** - (default: false) (conflicts with blacklist) - whether or not the structure is limited to specific biomes.
- **biomeBlackList** - (default: false) (conflicts with whitelist) - whether or not the structure is restricted from spawning within specific biomes.
- **biomes** - (default: minecraft:plains) - biomes to spawn in, if whitelist/blacklist is enabled.
- **maxDecay** - (default: 0) - percent value denoting the amount of possible integrity loss when spawning the structure. Higher values will increase missing blocks.
- **airSpawn** - (default: false) - whether or not the structure will only attempt spawning in air (as opposed to liquid). Conflicts with `liquidSpawn`, and ignored by `block` placement type. **Enable this if placement type is `surface`, or structure will also spawn in liquids.**
- **liquidSpawn** - (default: false) - whether or not the structure will only attempt spawning in liquids. Conflicts with `airSpawn`, and ignored by `block` placement type.
- **rotate** - (default: true) - if the structure will spawn with a random horizontal rotation.
- **mirror** - (default: true) - if the structure will spawn with a random horizontal mirror.
- **minY** - (default: -2147483648) - minimum Y to spawn the structure.
- **maxY** - (default: 2147483647) - maximum Y to spawn the structure.
- **minXZ** - (default: 0) - absolute value of minimum X OR Z coordinates, rounded by chunk.
- **maxXZ** - (default: 2147483647) - absolute value of maximum X OR Z coordinates, rounded by chunk.
- **placementType** - (default: surface) - Can either be `force`, `block`, `surface`, `liquidsurface`, or `ceiling`.
  - `force` will generate the structure anywhere and everywhere blocks can be placed, adhering to other spawn rules.
  - `block` will generate the structure within the block defined in `replaced`.
  - `surface` will generate the structure onto a flat walkable surface. This includes underwater/liquid surfaces if `airspawn` is not set to true.
  - `liquidsurface` will generate the structure atop liquid, if there is air above.
  - `ceiling` will generate the structure hanging from a flat ceiling. Recommended for terrain gen where this is a common occurrence. Otherwise, the ceilings of ravines and caves are the limit.
- **replaced** - (default: minecraft:air) - if using `block` placementType, set to `modid:yourblock`. The structure will spawn within said block. `replaced` is ignored if placementType is any value but `block`.
- **floating** - (default: false) - causes a structure with `block` placementType to raytrace ten blocks below it to determine if any blocks fail to match `replaced`. If so, the structure will retry spawning.
- **usesFillBlock** - (default: false) - if the foundations of a structure would otherwise be non-solid, fill them with a block. However, for performance and to avoid endless pillars, this will not occur if the space below exceeds 20 blocks. Functions with all placementTypes, but most recommended for `surface` and `ceiling`.
- **fillBlock** - (default: minecraft:dirt) - block to fill foundations, if `usesFillBlock` is enabled.
- **lenient** - (default: false) - if true, less obstruction checks will be performed to determine valid placement. Ideal for rough terrain where spawns would otherwise fail often, but will cause significantly more collisions with surroundings. At present, only `surface`, `liquidsurface`, and `ceiling` can use this.
- **enabled** - (default: false) - whether or not this structure will spawn. ***Set this to true once editing is finalized!***

#### Groups

To minimize complexity, .jsons will not properly generate variables automatically for groups; these parameters must be added to the structure yourself, after placing all the files of a grouping in a single directory. Groups use the spawnChance of a randomly-selected origin structure to determine frequency.

The following variables are ignored for non-origin structures in groups: `spawnChance`, `dimensions`, `biomes`, `minY`, and `maxY`, as they are assumed from the origin, or replaced by the parameters below.
#### Group parameters:

To create a structure grouping, add a `group.json` including the following parameters:
- **name** (default: "Unnamed Group") - name for the group.
- **origins** - (default: N/A, required) - an array of what structures (nbt filenames) can act as the start point for constructing the group. At least one structure within the directory must be designated, and the active origin will not be counted for minSize and maxSize.
- **maxSpreadXZ** - (default: 5) how many horizontal chunks distant from the origin/start point structures can spawn.
- **maxSpreadY** (default: 3) - how many vertical chunks distant from the origin/start point structures can spawn.
- **minSize** - (default: 8) - how many structures will generate within the group, prioritizing those with a minCount, followed by those with 0 minCount and weights.
- **maxSize** - (default: 12) - maximum quantity of structures to generate.
- **spacing** - (default: 2) - minimum distance from each other structures can spawn. Perceived very crudely.
- **enabled** - (default: true) - whether or not the group will generate at all.

##### Group structure parameters

Append these variables to each structure json, otherwise they will remain default. ***Groups will not generate without at least one origin.***
- **minCount** - (default:0) - how many of this structure *must* spawn. If greater than 0, will be prioritized over other structures regardless of weight. The active origin will always build regardless, and should remain 0 if more should not be generated. The sum of this value for all structures should not exceed `minSize` in the `group.json`.
- **maxCount** - (default: 2147483647) - the maximum quantity of this structure. All origin points should set this to 0 unless more should be placed, and the sum of this value for all structures must exceed or match the maximum size designated in the `group.json`. Set this to an absurd value if you wish it only to be limited by the group maximum. 
- **weight** - (default: 1) - the priority level of this structure; higher weights increase chance of generating as opposed to other structures in the group. Ignored once max count for this structure has been attained. If something is an origin or/and should only spawn its minimum count, set this to 0.

### Loading the structure

After tinkering with these file(s), reload the mod (/csreload) or game, and the structure should spawn as intended in new chunks.

### Links

[Modrinth](https://modrinth.com/mod/cubicstruct)

[GitLab](https://gitlab.com/Absthistle/cubicstruct)


### Additional Links

[CubicChunks (Modrinth)](https://modrinth.com/mod/cubicchunks)

[Cubic World Gen (Modrinth)](https://modrinth.com/mod/cwg)

[Iloot (Modrinth)](https://modrinth.com/mod/iloot)

### Contact

Contact the following for feedback and support; friend request may be required, but I will accept:

`Abstruse#8247` (Revolt)

`abstruse_paradox` (Discord)