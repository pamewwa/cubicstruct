package mod.puradox.cubicstruct.structure;

import mod.puradox.cubicstruct.CubicStruct;

import java.util.ArrayList;
import java.util.List;

public class StructureGroup {
    private List<StructureData> structures = new ArrayList<>(); //Set using StructureManager, rather than from deserialization. Spares .json tedium as it's rather obvious all files will arrive from the shared directory.
    private List<StructureData> origins;
    private String[] tempOrigins; //Origins are gathered before structures, thus must be initialized later.
    private String name;
    private int maxSpreadXZ;
    private int maxSpreadY;
    private int minSize;
    private int maxSize;
    private int spacing;

    private boolean enabled;

    public StructureGroup(String name, String[] origins) {
        this.name = name;
        this.tempOrigins=origins;

        this.maxSpreadXZ = 5;
        this.maxSpreadY = 3;

        this.minSize = 8;
        this.maxSize = 12;

        this.spacing = 2;

        this.enabled = true;
    }

    public String getName() {return name;}

    public StructureGroup setName(String name) {
        this.name = name;
        return this;
    }

    public List<StructureData> getStructures() {return structures;}

    public StructureGroup setStructures(List<StructureData> structures) {
        this.structures = structures;
        return this;
    }

    public List<StructureData> getOrigins() {return origins;}

    public StructureGroup setOrigins(List<StructureData> origins) {
        this.origins = origins;
        return this;
    }

    public void initializeOrigins() { //Called once all structures have been added.
        origins=new ArrayList<>();
        if(tempOrigins!=null) {
            for (StructureData structure : structures) {
                for (String tempOrigin : tempOrigins) {
                    if (structure.getNbtStructure().getName().equals(tempOrigin)) {
                        this.origins.add(structure);
                    }
                }
            }
            if(this.origins.isEmpty()) {CubicStruct.LOGGER.warn("\t\t\t{}{}", name, " has one or more origins, but none are valid; the structure will not generate.");}
        } else {
            CubicStruct.LOGGER.warn("\t\t\t{}{}", name, " has no origin; the structure will not generate.");
        } //Identify origins with their actual structures, from their filename.
    }

    public int getMaxSpreadXZ() {return maxSpreadXZ;}

    public StructureGroup setMaxSpreadXZ(int maxSpreadXZ) {
        this.maxSpreadXZ = maxSpreadXZ;
        return this;
    }

    public int getMaxSpreadY() {return maxSpreadY;}

    public StructureGroup setMaxSpreadY(int maxSpreadY) {
        this.maxSpreadY = maxSpreadY;
        return this;
    }

    public int getMinSize() {return minSize;}

    public StructureGroup setMinSize(int minSize) {
        this.minSize = minSize;
        return this;
    }

    public int getMaxSize() {return maxSize;}

    public StructureGroup setMaxSize(int maxSize) {
        if(minSize>maxSize) {
            CubicStruct.LOGGER.error("\t\t\t\t{}{}", name, " received inverted group size.");
        }
        this.maxSize = maxSize;
        return this;
    }

    public int getSpacing() {return spacing;}

    public StructureGroup setSpacing(int spacing) {
        this.spacing = spacing;
        return this;
    }

    public boolean isEnabled() {return enabled;}

    public StructureGroup setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }
}
