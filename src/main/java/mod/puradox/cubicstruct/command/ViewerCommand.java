package mod.puradox.cubicstruct.command;

import mcp.MethodsReturnNonnullByDefault;
import mod.puradox.cubicstruct.gui.ViewerScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;

import javax.annotation.ParametersAreNonnullByDefault;

@MethodsReturnNonnullByDefault
public class ViewerCommand extends CommandBase {
    @ParametersAreNonnullByDefault
    @Override
    public String getName() {return "csviewer";}

    @ParametersAreNonnullByDefault
    @Override
    public String getUsage(ICommandSender sender) {return "/csviewer Open the CubicStruct viewer UI.";}

    @ParametersAreNonnullByDefault
    @Override public void execute(MinecraftServer server, ICommandSender sender, String[] args) {
        if(sender.getCommandSenderEntity()!=null) { //Do not allow calling this directly from a command block. /execute may be fine.
            Minecraft.getMinecraft().displayGuiScreen(new ViewerScreen());
        }
    }

    @Override
    public int getRequiredPermissionLevel()
    {
        return 0;
    }
}
