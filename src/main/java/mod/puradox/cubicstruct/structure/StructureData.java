package mod.puradox.cubicstruct.structure;

import mod.puradox.cubicstruct.CubicStruct;
import net.minecraft.block.Block;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.structure.template.PlacementSettings;
import net.minecraft.world.gen.structure.template.Template;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class StructureData extends Template implements Serializable { //Object file containing both the .nbt and .json properties of a structure
    private final File nbtStructure;
    private String name;
    private String description;
    private double spawnChance;
    private int surfaceLevel;
    private int[] dimensions;
    private boolean usesBiomeWhiteList;
    private boolean usesBiomeBlackList;
    private Biome[] biomes;
    private int maxDecay;
    private boolean airSpawn;
    private boolean liquidSpawn;
    private int minY;
    private int maxY;
    private int minXZ;
    private int maxXZ;
    private boolean isMirrorable;
    private boolean isRotatable;
    private String placementType; //force, block, surface, liquidsurface, or ceiling

    private Block replaced;
    private boolean floating;

    private boolean usesFillBlock;
    private Block fillBlock;

    private boolean lenient;

    private boolean enabled;

    private final PlacementSettings SETTINGS;


    //Settings for groups/complexes
    private int minCount;
    private int maxCount;
    private int weight;
    //Settings for groups/complexes

    public StructureData(File nbtStructure) throws IOException {
        //See README for information on variables. These are defaults to be overwritten.
        this.nbtStructure = nbtStructure;
        this.name = FilenameUtils.removeExtension(nbtStructure.getName());
        this.description = "No description provided.";
        this.spawnChance = 1;
        this.surfaceLevel = 0;
        this.dimensions = new int[]{0};
        this.usesBiomeWhiteList = false;
        this.usesBiomeBlackList = false;
        this.biomes = new Biome[]{Biomes.PLAINS};
        this.maxDecay = 0;
        this.airSpawn = false;
        this.liquidSpawn = false;
        this.minY = Integer.MIN_VALUE;
        this.maxY = Integer.MAX_VALUE;
        this.minXZ = 0;
        this.maxXZ = Integer.MAX_VALUE;
        this.isRotatable = true;
        this.isMirrorable = true;
        this.placementType="surface";
        this.replaced=Blocks.AIR;
        this.floating=false;
        this.usesFillBlock=false;
        this.fillBlock=Blocks.DIRT;
        this.lenient=false;
        this.enabled = false;

        read(StructureManager.fileToNBT(nbtStructure)); //Sets all nbt-related variables, such as block and entity lists.
        SETTINGS = new PlacementSettings()
                .setIntegrity((100-maxDecay)/100F)
                .setIgnoreEntities(false);

        this.minCount = 0;
        this.maxCount = Integer.MAX_VALUE;
        this.weight = 1;
    }


    public File getNbtStructure() {
        return nbtStructure;
    }

    public String getName() {return name;}

    public StructureData setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {return description;}

    public StructureData setDescription(String description) {
        this.description = description;
        return this;
    }

    public double getSpawnChance() {return spawnChance;}

    public StructureData setSpawnChance(double spawnChance) {
        if(spawnChance<=0) {
            CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " has no chance of spawning.");
        } else if (spawnChance > 99) {
            CubicStruct.LOGGER.info("\t\t\t\t{}{}", this.name, " will attempt spawning in every chunk. Is this okay?");
        }

        this.spawnChance = spawnChance;
        return this;}

    public int getSurfaceLevel() {return surfaceLevel;}

    public StructureData setSurfaceLevel(int surfaceLevel) {
        this.surfaceLevel = surfaceLevel;
        return this;
    }

    public int[] getDimensions() {return dimensions;}

    public StructureData setDimensions(int[] dimensions) {
        this.dimensions = dimensions;
        return this;
    }

    public boolean usesBiomeWhiteList() {return usesBiomeWhiteList;}

    public StructureData setUsesBiomeWhiteList(boolean usesBiomeWhiteList) {
        this.usesBiomeWhiteList = usesBiomeWhiteList;
        if(usesBiomeBlackList && usesBiomeWhiteList) {
            CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " uses conflicting biome lists and may not generate properly.");
        }
        return this;
    }

    public boolean usesBiomeBlackList() {return usesBiomeBlackList;}

    public StructureData setUsesBiomeBlackList(boolean usesBiomeBlackList) {
        this.usesBiomeBlackList = usesBiomeBlackList;
        if(usesBiomeBlackList && usesBiomeWhiteList) {
            CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " uses conflicting biome lists and may not generate properly.");
        }
        return this;
    }

    public Biome[] getBiomes() {return biomes;}

    public StructureData setBiomes(Biome[] biomes) {
        this.biomes = biomes;
        return this;
    }

    public int getMaxDecay() {return maxDecay;}

    public StructureData setMaxDecay(int maxDecay) {
            if(maxDecay<0) {
                CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " has negative decay. Assuming none instead.");

                this.maxDecay = 0;
                return this;
            } else if (maxDecay > 99) {
                CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " will spawn with 0 integrity!");
            }
        this.maxDecay = maxDecay;
        SETTINGS.setIntegrity((100-maxDecay)/100F);
        return this;
    }

    public boolean isAirSpawn() {return airSpawn;}

    public StructureData setAirSpawn(boolean airSpawn) {
        this.airSpawn = airSpawn;
        if(airSpawn && liquidSpawn) {
            CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " is both air and liquid-spawning, and may not generate properly.");
        }
        return this;
    }

    public boolean isLiquidSpawn() {return liquidSpawn;}

    public StructureData setLiquidSpawn(boolean liquidSpawn) {
        this.liquidSpawn = liquidSpawn;
        if(airSpawn && liquidSpawn) {
            CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " is both air and liquid-spawning, and may not generate properly.");
        }
        return this;
    }

    public int getMinY() {return minY;}

    public StructureData setMinY(int minY) {
        this.minY = minY;
        if(minY>maxY) {CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " is defined with inverted Y parameters, and will not generate.");}
        return this;
    }

    public int getMaxY() {return maxY;}

    public StructureData setMaxY(int maxY) {
        this.maxY = maxY;
        if(minY>maxY) {CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " is defined with inverted Y parameters, and will not generate.");}
        return this;
    }

    public int getMinXZ() {return minXZ;}

    public StructureData setMinXZ(int minXZ) {
        this.minXZ = minXZ;
        if(minXZ>maxXZ) {CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " is defined with inverted XZ parameters, and will not generate.");}
        return this;
    }

    public int getMaxXZ() {return maxXZ;}

    public StructureData setMaxXZ(int maxXZ) {
        this.maxXZ = maxXZ;
        if(minXZ>maxXZ) {CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " is defined with inverted XZ parameters, and will not generate.");}
        return this;
    }

    public boolean isMirrorable() {return isMirrorable;}

    public StructureData setMirrorable(boolean mirrorable) {
        isMirrorable = mirrorable;
        if(!mirrorable) {SETTINGS.setMirror(Mirror.NONE);}
        return this;
    }

    public boolean isRotatable() {return isRotatable;}

    public StructureData setRotatable(boolean rotatable) {
        isRotatable = rotatable;
        if(!rotatable) {SETTINGS.setRotation(Rotation.NONE);}
        return this;
    }

    public String getPlacementType() {return placementType;}

    public StructureData setPlacementType(String placementType) throws Exception {
        if (placementType.equals("force") || placementType.equals("block") || placementType.equals("surface") || placementType.equals("liquidsurface") || placementType.equals("ceiling")) {
            this.placementType=placementType;
        } else {
            throw new Exception("Structure '" + name + "' failed to load. Valid candidates for 'placementType' are 'force,' 'block,' 'surface,' 'liquidsurface,' and 'ceiling!'");
        } // Be sure placementType is valid, and assist in identifying the error otherwise. Also, better to crash the game sooner rather than later.
        return this;
    }

    public Block getReplaced() {return replaced;}

    public StructureData setReplaced(Block replaced) {
        this.replaced = replaced;
        return this;
    }

    public boolean isFloating() {return floating;}

    public StructureData setFloating(boolean floating) {
        this.floating = floating;
        return this;
    }

    public boolean usesFillBlock() {return usesFillBlock;}

    public StructureData setUsesFillBlock(boolean usesFillBlock) {
        this.usesFillBlock = usesFillBlock;
        return this;
    }

    public Block getFillBlock() {return fillBlock;}

    public StructureData setFillBlock(Block fillBlock) {
        this.fillBlock = fillBlock;
        return this;
    }

    public boolean isLenient() {return lenient;}

    public StructureData setLenient(boolean lenient) {
        this.lenient = lenient;
        return this;
    }

    public boolean isEnabled() {return enabled;}

    public StructureData setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public PlacementSettings getSettings() {
        return SETTINGS.copy();
    }

    public int getMinCount() {return minCount;}

    public StructureData setMinCount(int minCount) {
        if(minCount<0) {
            CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " received a minCount less than 0. Assuming 0.");
            this.minCount = 0;
            return this;
        } else if (minCount>maxCount && maxCount!=0) {
            CubicStruct.LOGGER.error("\t\t\t\t{}{}", this.name, " received a greater minCount than maxCount!");
        }
        this.minCount = minCount;
        return this;
    }

    public int getMaxCount() {return maxCount;}

    public StructureData setMaxCount(int maxCount) {
        if(maxCount<0) {
            CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " received a maxCount less than 0. Assuming infinity.");
            this.maxCount = Integer.MAX_VALUE;
            return this;
        } else if (maxCount < minCount && maxCount!=0) {
            CubicStruct.LOGGER.error("\t\t\t\t{}{}", this.name, " received a greater minCount than maxCount!");
        }
        this.maxCount = maxCount;
        return this;
    }

    public int getWeight() {return weight;}

    public StructureData setWeight(int weight) {
        if(weight<0) {
            CubicStruct.LOGGER.warn("\t\t\t\t{}{}", this.name, " received a negative weight. Assuming 0.");
            this.weight = 0;
            return this;
        }
        this.weight = weight;
        return this;
    }
}
