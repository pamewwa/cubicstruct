package mod.puradox.cubicstruct.worldgen;

import mod.puradox.cubicstruct.CubicStruct;
import mod.puradox.cubicstruct.structure.StructureData;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.VillagerRegistry;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.Random;

public class GeneratorUtils { //Various utility methods for generating structures.

    public static Mirror randomMirror(Random rand) { //Return random mirror according to seed.
        int mirrorInt = rand.nextInt(3);
        if(mirrorInt==0){return Mirror.NONE;}
        if(mirrorInt==1){return Mirror.FRONT_BACK;}
        return Mirror.LEFT_RIGHT;
    }

    public static Rotation randomRotation(Random rand) { //Return random rotation according to seed.
        int mirrorInt = rand.nextInt(4);
        if(mirrorInt==0){return Rotation.NONE;}
        if(mirrorInt==1){return Rotation.CLOCKWISE_90;}
        if(mirrorInt==2){return Rotation.CLOCKWISE_180;}
        return Rotation.COUNTERCLOCKWISE_90;
    }

    public static void fillFoundation(World world, BlockPos pos, BlockPos size, IBlockState fillBlock, boolean ceiling) { //Assumes position is positively outwards from X and Z, and that surfaceLevel is accounted for.
        int i = 1;
        int reverse = ceiling ? 1:-1; //Ceiling would fill upwards.
        pos = ceiling ? pos.add(0,size.getY(),0):pos; //Start from top of bounds if ceiling.
        //System.out.println("Checking air at " + pos + " to " + pos.add(size.getX(), 0, size.getZ()));
        while(i<21) { //Find the nearest position where all four corners are solid.
            if(world.getBlockState(pos.add(0,i*reverse,0)).getMaterial().blocksMovement() //Start pos surface check.
                    && world.getBlockState(pos.add(size.getX(),i*reverse,0)).getMaterial().blocksMovement()
                    && world.getBlockState(pos.add(0,i*reverse,size.getZ())).getMaterial().blocksMovement()
                    && world.getBlockState(pos.add(size.getX(), i*reverse, size.getZ())).getMaterial().blocksMovement() //Opposite end surface check.
            ) {
                break;
            }
            i++;
        }
        if(i==21) {return;} //Don't bother if it cannot be filled within 20 blocks.
        if(ceiling) {
            for (int x = pos.getX(); x < pos.getX() + size.getX(); x++) {
                for (int y = pos.getY(); y < pos.getY() + i; y++) {
                    for (int z = pos.getZ(); z < pos.getZ() + size.getZ(); z++) {
                        BlockPos p = new BlockPos(x, y, z);
                        if(!world.getBlockState(p).getMaterial().blocksMovement()) { //Fill if air.
                            world.setBlockState(p, fillBlock);
                        }
                    }
                }
            }
        } else {
            for (int x = pos.getX(); x < pos.getX() + size.getX(); x++) {
                for (int y = pos.getY()-Math.max(i,1); y < pos.getY(); y++) {
                    for (int z = pos.getZ(); z < pos.getZ() + size.getZ(); z++) {
                        BlockPos p = new BlockPos(x, y, z);
                        if(!world.getBlockState(p).getMaterial().blocksMovement()) { //Fill if air.
                            world.setBlockState(p, fillBlock);
                        }
                    }
                }
            }
        }
    }

    public static boolean isSurfaceValid(World world, StructureData structure, BlockPos pos, Rotation rotation, Mirror mirror, boolean ceiling) {
        BlockPos size = structure.transformedSize(rotation); //Not actually a position. Confused me too, but now that I consider it, quite a convenient means to store x, y, and z size.
        //System.out.println(pos.toString() + world.getBlockState(pos).getBlock());
        pos = getStartPos(pos, size, rotation, mirror); //Line up where the structure will be spawned, with +x and +z.
        //System.out.println(pos.toString() + world.getBlockState(pos).getBlock());
        if(world.getBlockState(pos).getMaterial().blocksMovement() //Start pos surface check.
                && world.getBlockState(pos.add(size.getX(), 0, size.getZ())).getMaterial().blocksMovement() //Opposite end surface check.
        ) {
            if(ceiling) {
                //Is the centre obstructed? Ignored if lenient.
                return !world.getBlockState(pos.add(size.getX() / 2, (-(size.getY() + (-structure.getSurfaceLevel()>=size.getY()?-structure.getSurfaceLevel()+1:structure.getSurfaceLevel()))) + (structure.isLenient() ? 1 + (structure.getSize().getY()) / 12 : 0), size.getZ() / 2)).getMaterial().blocksMovement() //Is floor obstructing?
                        && world.getBlockState(pos.add(size.getX() / 2, 0, size.getZ() / 2)).getMaterial().blocksMovement() //Top middle surface check.
                        && (structure.isLenient() || !world.getBlockState(pos.add(size.getX() / 2, -1, size.getZ() / 2)).getMaterial().blocksMovement());
            } else if (!world.getBlockState(pos.add(size.getX() / 2, (size.getY() + (-structure.getSurfaceLevel()>=size.getY()?-structure.getSurfaceLevel()+1:structure.getSurfaceLevel())) - (structure.isLenient() ? 1+(structure.getSize().getY())/12:0), size.getZ() / 2)).getMaterial().blocksMovement() //Is ceiling obstructing?
                    && ((!structure.isLiquidSpawn()) || world.getBlockState(pos.add(size.getX() / 2, size.getY(), size.getZ() / 2)).getMaterial().isLiquid()) //Be sure liquid-spawning structures won't spawn with their tops uncovered.
                    && world.getBlockState(pos.add(size.getX()/2+1, 0, size.getZ()/2+1)).getMaterial().blocksMovement() //Bottom middle surface check.
                    && (structure.isLenient() || !world.getBlockState(pos.add(size.getX() / 2, 1, size.getZ() / 2)).getMaterial().blocksMovement())) { //Is the centre obstructed? Ignored if lenient.
                return true;
            }
        } //Not checking other side for obstructions. For large structures, it would dramatically reduce spawns for only a small aesthetic boost.
        return false;
    }

    public static boolean isWaterSurfaceValid(World world, StructureData structure, BlockPos pos, Rotation rotation, Mirror mirror) {
        BlockPos size = structure.transformedSize(rotation); //Not actually a position. Confused me too, but now that I consider it, quite a convenient means to store x, y, and z size.
        pos = getStartPos(pos, size, rotation, mirror); //Line up where the structure will be spawned, with +x and +z.
        if(world.getBlockState(pos).getMaterial().isLiquid() //Start pos surface check
                && world.getBlockState(pos.add(size.getX(), 0, size.getZ())).getMaterial().isLiquid() //Opposite end surface check.
                && world.getBlockState(pos.add(size.getX()/2, 0, size.getZ()/2)).getMaterial().isLiquid() //Bottom middle surface check.
        ) {
            return !world.getBlockState(pos.add(size.getX() / 2, (size.getY()+(-structure.getSurfaceLevel()>=size.getY()?-structure.getSurfaceLevel()+1:structure.getSurfaceLevel()))-(structure.isLenient() ? 1+(structure.getSize().getY())/12:0), size.getZ() / 2)).getMaterial().blocksMovement() //Is ceiling obstructing?
                    && !world.getBlockState(pos.add((size.getX()-1) / 2, 1, (size.getZ()-1) / 2)).getMaterial().blocksMovement(); //Is other end obstructed?

        }
        return false;
    }

    public static boolean isFloating(World world, BlockPos pos, Block validBlock) {
        for(int i = -1; i > -10; i--) { //Iterate ten blocks downwards.
            if(!world.getBlockState(pos.add(0,i,0)).getBlock().equals(validBlock)) { //If an invalid block is detected, it isn't floaty enough.
                return false;
            }
        }
        return true;
    }

    public static boolean checkStructureCollision(BlockPos selfPos, BlockPos selfSize, Map<BlockPos,BlockPos> others, boolean lenient, boolean ceiling, int surfaceLevel) { //Assumes position is positively outwards from X and Z.
        BlockPos selfEnd;
        BlockPos selfBottomEnd;
        BlockPos selfBottomX;
        BlockPos selfBottomZ;

        //Bottom positive Z corner.
        //Bottom positive X corner.
        //Opposite bottom end.
        if(!ceiling) {
            selfEnd = selfPos.add(selfSize.getX(), selfSize.getY() + surfaceLevel, selfSize.getZ()); //Opposite end.
        } else {
            selfEnd = selfPos.add(selfSize.getX(), selfSize.getY() - surfaceLevel, selfSize.getZ()); //Opposite end.
        }
        selfBottomEnd = selfPos.add(selfSize.getX(), 1, selfSize.getZ()); //Opposite bottom end.
        selfBottomX = selfPos.add(selfSize.getX(), 1, 0); //Bottom positive X corner.
        selfBottomZ = selfPos.add(0, 1, selfSize.getZ()); //Bottom positive Z corner.

        boolean[] valid = {true};
        others.forEach((pos, size) -> { //Determine if any of the current structure's points are within another's of the group.
            if(((       selfPos.getX() > pos.getX() && selfPos.getX() < pos.getX()+size.getX()) //Check startpoint.
                    && (selfPos.getY() > pos.getY() && selfPos.getY() < pos.getY()+size.getY())
                    && (selfPos.getZ() > pos.getZ() && selfPos.getZ() < pos.getZ()+size.getZ()))
                    || ((selfEnd.getX() > pos.getX() && selfEnd.getX() < pos.getX()+size.getX()) //Opposite end.
                    && (selfEnd.getY() > pos.getY() && selfEnd.getY() < pos.getY()+size.getY())
                    && (selfEnd.getZ() > pos.getZ() && selfEnd.getZ() < pos.getZ()+size.getZ()))
                    || ((selfBottomEnd.getX() > pos.getX() && selfBottomEnd.getX() < pos.getX()+size.getX()) //Opposite bottom end
                    && (selfBottomEnd.getY() > pos.getY() && selfBottomEnd.getY() < pos.getY()+size.getY())
                    && (selfBottomEnd.getZ() > pos.getZ() && selfBottomEnd.getZ() < pos.getZ()+size.getZ()))
                    || ((selfBottomX.getX() > pos.getX() && selfBottomX.getX() < pos.getX()+size.getX()) //Bottom positive X corner.
                    && (selfBottomX.getY() > pos.getY() && selfBottomX.getY() < pos.getY()+size.getY())
                    && (selfBottomX.getZ() > pos.getZ() && selfBottomX.getZ() < pos.getZ()+size.getZ()))
                    || ((selfBottomZ.getX() > pos.getX() && selfBottomZ.getX() < pos.getX()+size.getX()) //Bottom positive Z corner.
                    && (selfBottomZ.getY() > pos.getY() && selfBottomZ.getY() < pos.getY()+size.getY())
                    && (selfBottomZ.getZ() > pos.getZ() && selfBottomZ.getZ() < pos.getZ()+size.getZ())))
                {
                valid[0] = false;
            }
        });
        return valid[0];
    }

    public static BlockPos getStartPos(BlockPos pos, BlockPos size, Rotation rotation, Mirror mirror) { //Align spawning position with positive X and Z, according to mirror and rotation.
        int m = 0;
        if(mirror.ordinal()==0) {m=0;}
        else if(mirror.ordinal()==1) {m=-1;}
        else if(mirror.ordinal()==2) {m=1;}//Re-arrange mirror ordinals for better adding.

        int rotation_inc = rotation.ordinal()+m; //The increments of 90 the structure will be rotated.
        BlockPos newPos;
        if(rotation_inc==-1) {newPos=pos.add(0, 0, -size.getZ());}
        else if(rotation_inc==0) {newPos=pos;}
        else if(rotation_inc==1) {newPos=pos.add(-size.getX(), 0, 0);}
        else if(rotation_inc==2) {newPos=pos.add(-size.getX(), 0, -size.getZ());}
        else if(rotation_inc==3) {newPos=pos.add(0, 0, -size.getZ());}
        else if(rotation_inc==4) {newPos=pos;}
        else {
            CubicStruct.LOGGER.error("Structure attempted to spawn with out-of-bounds rotation increment?");
            newPos=pos;
        }
        return newPos;
    }

    public static void randomVillager(World world, BlockPos pos, Random rand, @Nullable VillagerRegistry.VillagerProfession profession, @Nullable Integer career) {
        EntityVillager villager = new EntityVillager(world);
        villager.setLocationAndAngles(pos.getX()+0.5, pos.getY(), pos.getZ()+0.5, 0.0F, 0.0F);
        if(profession == null) {
            net.minecraftforge.fml.common.registry.VillagerRegistry.setRandomProfession(villager, rand);
        } else {
            villager.setProfession(profession);
        }
        villager.finalizeMobSpawn(world.getDifficultyForLocation(new BlockPos(villager)), null, false);
        if (career != null) {
            villager.careerId=career;
            villager.buyingList=null;
        }
        world.spawnEntity(villager);

    }
}
