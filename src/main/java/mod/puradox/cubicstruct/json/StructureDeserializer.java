package mod.puradox.cubicstruct.json;

import com.google.gson.*;
import mod.puradox.cubicstruct.structure.StructureData;
import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public final class StructureDeserializer implements JsonDeserializer<StructureData> {
    @Override
    public StructureData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject obj = json.getAsJsonObject(); //Acquire the object we're deserializing.

        //First, acquire all finnicky variables.
        List<Integer> iteratedDimensions = new ArrayList<>();
        int[] dimensions = new int[]{0};
        if (obj.get("dimensions") != null) {
            for (JsonElement jsonItem : obj.getAsJsonArray("dimensions")) {
                iteratedDimensions.add(jsonItem.getAsInt());
            }
            dimensions = new int[iteratedDimensions.size()];
            for(int i = 0; i < iteratedDimensions.size(); i++) {
                dimensions[i] = iteratedDimensions.get(i);
            }
        }

        List<Biome> iteratedBiomes = new ArrayList<>();
        Biome[] biomes = new Biome[]{Biome.REGISTRY.getObject(new ResourceLocation("minecraft:plains"))};
        if (obj.get("biomes") != null) {
            for (JsonElement jsonItem : obj.getAsJsonArray("biomes")) {
                iteratedBiomes.add(Biome.REGISTRY.getObject(new ResourceLocation(jsonItem.getAsString())));
            }
            biomes = new Biome[iteratedBiomes.size()];
            for(int i = 0; i < iteratedBiomes.size(); i++) {
                biomes[i] = iteratedBiomes.get(i);
            }
        }

        Block replaced = null;
        if(obj.get("replaced")!=null) {
            replaced = Block.REGISTRY.getObject(new ResourceLocation(obj.get("replaced").getAsString()));
        }

        Block fillBlock = null;
        if(obj.get("fillBlock")!=null) {
            fillBlock = Block.REGISTRY.getObject(new ResourceLocation(obj.get("fillBlock").getAsString()));
        }


        try { //Initialize structureData.
            StructureData data = new StructureData(new File(obj.get("file").getAsString()));

            data.setEnabled(obj.get("enabled").getAsBoolean());
            if(obj.get("name")!=null){data.setName(obj.get("name").getAsString());}
            if(obj.get("description")!=null){data.setDescription(obj.get("description").getAsString());}
            if(obj.get("spawnChance")!=null){data.setSpawnChance(obj.get("spawnChance").getAsDouble());}
            data.setDimensions(dimensions);
            if(obj.get("surfaceLevel")!=null){data.setSurfaceLevel(obj.get("surfaceLevel").getAsInt());}
            if(obj.get("biomeWhiteList")!=null){data.setUsesBiomeWhiteList(obj.get("biomeWhiteList").getAsBoolean());}
            if(obj.get("biomeBlackList")!=null){data.setUsesBiomeBlackList(obj.get("biomeBlackList").getAsBoolean());}
            data.setBiomes(biomes);
            if(obj.get("maxDecay")!=null){data.setMaxDecay(obj.get("maxDecay").getAsInt());}
            if(obj.get("airSpawn")!=null){data.setAirSpawn(obj.get("airSpawn").getAsBoolean());}
            if(obj.get("liquidSpawn")!=null){data.setLiquidSpawn(obj.get("liquidSpawn").getAsBoolean());}
            if(obj.get("minY")!=null){data.setMinY(obj.get("minY").getAsInt());}
            if(obj.get("maxY")!=null){data.setMaxY(obj.get("maxY").getAsInt());}
            if(obj.get("minXZ")!=null){data.setMinXZ(obj.get("minXZ").getAsInt());}
            if(obj.get("maxXZ")!=null){data.setMaxXZ(obj.get("maxXZ").getAsInt());}
            if(obj.get("rotate")!=null){data.setRotatable(obj.get("rotate").getAsBoolean());}
            if(obj.get("mirror")!=null){data.setMirrorable(obj.get("mirror").getAsBoolean());}
            if(obj.get("placementType")!=null){data.setPlacementType(obj.get("placementType").getAsString());}
            if(obj.get("replaced")!=null){data.setReplaced(replaced);}
            if(obj.get("floating")!=null){data.setFloating(obj.get("floating").getAsBoolean());}
            if(obj.get("usesFillBlock")!=null){data.setUsesFillBlock(obj.get("usesFillBlock").getAsBoolean());}
            if(obj.get("fillBlock")!=null){data.setFillBlock(fillBlock);}
            if(obj.get("lenient")!=null){data.setLenient(obj.get("lenient").getAsBoolean());}

            if(obj.get("minCount")!=null){data.setMinCount(obj.get("minCount").getAsInt());}
            if(obj.get("maxCount")!=null){data.setMaxCount(obj.get("maxCount").getAsInt());}
            if(obj.get("weight")!=null){data.setWeight(obj.get("weight").getAsInt());}

            return data;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
