package mod.puradox.cubicstruct.gui;

import mod.puradox.cubicstruct.structure.StructureData;

import net.minecraft.client.gui.GuiButton;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public final class StructureElement { //Disposable 'clickable' element in ViewerScreen.
    private final StructureData data;
    private final String groupName;

    private int pos; //Slot to appear in. Initialized on ViewerScreen's `reloadEntries()`.
    private final int id;
    private boolean visible = false; //Is entry visible? To cull entry if outside scroll.

    private static final int ENTRY_WIDTH = 79;
    private static final int ENTRY_HEIGHT = 31;

    private int x;
    private int y;
    private int centreX;
    private int centreY; //Unused for now, until more buttons are implemented.
    private final ViewerScreen screen;
    private GuiButton selectArea;

    private final List<GuiButton> activeButtons = new ArrayList<>();


    public StructureElement(StructureData data, ViewerScreen screen, int id, @Nullable String groupName) {
        this.data = data;
        this.screen = screen;
        this.id = id;

        this.groupName = groupName == null ? "":groupName; //Empty is better than nullpointer.

        this.x = ViewerScreen.CENTRE_X+17;
        this.y = ViewerScreen.CENTRE_Y-6+(id*ENTRY_HEIGHT);

        this.centreX = x+ENTRY_WIDTH/2;
        this.centreY = y+ENTRY_HEIGHT/2;

        initButtons();
    }

    private void initButtons() {
        selectArea = new CubicToggleButton(pos, x, y, data.getName());
    }
    public void select(boolean b) {
        ((CubicToggleButton)selectArea).toggle = b;
    }

    public void setVisible(boolean b) {
        this.visible=b;
        if(b) {
            activeButtons.forEach(button -> button.visible=false);
            activeButtons.clear();
            activeButtons.add(selectArea);
            activeButtons.forEach(button -> button.visible=true);
        } else {
            activeButtons.forEach(button -> button.visible=false);
            activeButtons.clear();
        }
    }

    public void updateScreenPosition() {
        this.x = ViewerScreen.CENTRE_X+17;
        this.y = ViewerScreen.CENTRE_Y-6+(pos*ENTRY_HEIGHT);
        selectArea.x=this.x;
        selectArea.y=this.y;
    }

    public boolean isVisible(){return visible;}

    public StructureData getStructure() {
        return data;
    }

    public String getGroupName() {return groupName;}

    public void move(int newPos) {
        pos = newPos;
        updateScreenPosition();
    }

    public int getId() {return id;}

    public int getPos() {return pos;}

    public List<GuiButton> getActiveButtons() {
        return activeButtons;
    }

    public GuiButton getSelectArea() {
        return selectArea;
    }
}
